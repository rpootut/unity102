using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararController : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;

    [SerializeField]
    private GameObject punto;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameObject bala = Instantiate(prefab);

            bala.transform.position = punto.transform.position;

            Destroy(bala, 5);
        }        

    }
}
